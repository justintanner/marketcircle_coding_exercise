require 'socket'

MAX_TCP_BYTES = 1500
HOST = 'interview.marketcircle.zone'
PORT = 9000

def send_command(name, key=nil, value=nil)
  name.upcase!

  payload = name.ljust(8, ' ').chars.map(&:ord)

  payload = payload + encode_string(key) unless key.nil?
  payload = payload + encode_string(value) unless value.nil?

  socket = TCPSocket.new HOST, PORT

  socket.write payload.pack('C*')

  status_code = socket.recv(1).bytes.first

  if expect_response(name) && status_code == 1
    response_bytes = socket.recv(1500).bytes
  end

  socket.close

  return status_code, response_bytes
end

def encode_int64(number)
  [number].pack('q<').unpack('C*')
end

def decode_int64(bytes)
  bytes.pack('C*').unpack('q<').first
end

def encode_string(string)
  return [] if string.length == 0

  bytes = encode_int64(string.length)

  bytes = bytes + string.chars.map(&:ord)

  bytes
end

def decode_string(bytes)
  return nil if bytes.length < 8

  string_length = decode_int64(bytes[0, 8])

  return bytes[8..-1].pack('C*') if string_length > 0
end

def decode_array_of_strings(bytes)
  array_size = decode_int64(bytes[0, 8])

  encoded_strings = bytes[8..-1]
  decoded_strings = []

  array_size.times do
    size_bytes = encoded_strings[0, 8]
    size = decode_int64(size_bytes)
    string_bytes = encoded_strings[8, size]

    decoded_strings << decode_string(size_bytes + string_bytes)

    encoded_strings.shift(8 + size)
  end

  decoded_strings
end

def encode_array_of_strings(array)
  encoded_bytes = encode_int64(array.length)

  array.each do |string|
    encoded_bytes = encoded_bytes + encode_string(string)
  end

  encoded_bytes
end

def expect_response(command)
  !['HI!', 'PUT', 'DELETE'].include?(command)
end

def is_valid_command(command)
  ['HI!', 'COUNT', 'KEYS', 'PUT', 'GET', 'DELETE'].include?(command.to_s.upcase)
end
