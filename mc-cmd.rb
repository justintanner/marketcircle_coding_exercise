#!/usr/bin/env ruby

require_relative 'mc_api'

if ARGV[0].nil? && is_valid_command(ARGV[0])
  puts 'Usage: mc-cmd hi!|count|keys|put|get|delete ["key"] ["value"]'
  exit(1)
end

command = ARGV[0].upcase
key = ARGV[1].gsub('"', '') unless ARGV[1].nil?
value = ARGV[2].gsub('"', '') unless ARGV[2].nil?

status_code, response_bytes = send_command(command, key, value)

if status_code == 0
  puts "Invalid command or server error"
  exit(1)
end

case command
when 'HI!'
  puts 'HI!'
when 'COUNT'
  puts decode_int64(response_bytes)
when 'KEYS'
  puts decode_array_of_strings(response_bytes).join("\n")
when 'PUT'
  puts 'Done!'
when 'GET'
  puts decode_string(response_bytes)
when 'DELETE'
  puts 'Done!'
end

exit(0)
