require 'minitest'
require 'minitest/autorun'
require_relative 'mc_api'

class McApiTest < MiniTest::Test

  def test_hi
    status_code, response_bytes = send_command('HI!')

    assert_equal 1, status_code
  end

  def test_count
    status_code, response_bytes = send_command('COUNT')

    assert_equal Fixnum, response_bytes.first.class
  end

  def test_keys
    status_code, response_bytes = send_command('KEYS')

    assert_equal Array, response_bytes.class
  end

  def test_put
    status_code, response_bytes = send_command('PUT', 'ping', 'pong')

    assert_equal 1, status_code
  end

  def test_get
    status_code, response_bytes = send_command('GET', 'not there!')

    assert_equal 0, status_code
  end

  # TODO: Figure out why this is not getting a response from the server
  # def test_delete
  #   status_code, response_bytes = send_command('DELETE', 'not there!')

  #   assert_equal 0, status_code
  # end

  def test_encode_int64
    assert_equal [255, 0, 0, 0, 0, 0, 0, 0], encode_int64(255)
  end

  def test_decode_int64
    assert_equal 3, decode_int64([3, 0, 0, 0, 0, 0, 0, 0])
  end

  def test_encoding_negatives
    assert_equal -199, decode_int64(encode_int64(-199))
  end

  def test_decode_string
    assert 3, decode_string([0x10, 0, 0, 0, 0, 0, 0, 0])
  end

  def test_encode_string
    assert_equal [1, 0, 0, 0, 0, 0, 0, 0, 65], encode_string("A")
  end

  def test_decode_string
    encoded_bytes = encode_string("Oh hai der!")

    assert_equal "Oh hai der!", decode_string(encoded_bytes)
  end

  def test_array_of_strings
    array_of_strings = ['there', 'can', 'be', 'only', 'one']

    encoded_bytes = encode_array_of_strings(array_of_strings)

    assert_equal array_of_strings, decode_array_of_strings(encoded_bytes)
  end

end
