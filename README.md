# Coding Exercise for Marketcircle by Justin Tanner

The following is my solution for the coding exercise layed out here:

https://github.com/Marketcircle/backend-software-developer-coding-exercise/blob/master/README.md

I wrote a small ruby script to interact with the API and a few tests to go along with it.

## Command

Install ruby and execute to usage information:

```
ruby mc-cmd.rb
```

Here is an example of putting a new key to the server:

```
ruby mc-cmd.rb put ping 1
```

And getting your value back

```
ruby mc-cmd.rb get ping
```

Output should be:

```
1
```

## Tests

I wrote a handful of tests to get things going, to run the tests execute:

```
ruby test.rb
```

Justin Tanner.
